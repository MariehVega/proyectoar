
var scene, sceneGroup, camera, renderer, clock, deltaTime, totalTime;

var arToolkitSource, arToolkitContext;

var markerRoot1, markerRoot2, markerRoot3, markerRoot4;

var mesh0, mesh1, mesh2, mesh3;

controlsOn = false;

function webGLStart() {
	initialize();
	animate();
}

function initialize() {
	scene = new THREE.Scene();

	let ambientLight = new THREE.AmbientLight(0xcccccc, 0.5);
	scene.add(ambientLight);

	camera = new THREE.Camera();
	scene.add(camera);

	renderer = new THREE.WebGLRenderer({
		antialias: true,
		alpha: true
	});
	renderer.setClearColor(new THREE.Color('lightgrey'), 0)
	renderer.setSize(640, 480);
	renderer.domElement.style.position = 'absolute'
	renderer.domElement.style.top = '0px'
	renderer.domElement.style.left = '0px'
	document.body.appendChild(renderer.domElement);
	renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.PCFSoftShadowMap;

	clock = new THREE.Clock();
	deltaTime = 0;
	totalTime = 0;

	////////////////////////////////////////////////////////////
	// setup arToolkitSource
	////////////////////////////////////////////////////////////

	arToolkitSource = new THREEx.ArToolkitSource({
		sourceType: 'webcam',
	});

	function onResize() {
		arToolkitSource.onResize()
		arToolkitSource.copySizeTo(renderer.domElement)
		if (arToolkitContext.arController !== null) {
			arToolkitSource.copySizeTo(arToolkitContext.arController.canvas)
		}
	}

	arToolkitSource.init(function onReady() {
		onResize()
	});

	// handle resize event
	window.addEventListener('resize', function () {
		onResize()
	});

	////////////////////////////////////////////////////////////
	// setup arToolkitContext
	////////////////////////////////////////////////////////////	

	// create atToolkitContext
	arToolkitContext = new THREEx.ArToolkitContext({
		cameraParametersUrl: 'data/camera_para.dat',
		detectionMode: 'mono'
	});

	// copy projection matrix to camera when initialization complete
	arToolkitContext.init(function onCompleted() {
		camera.projectionMatrix.copy(arToolkitContext.getProjectionMatrix());
	});

	////////////////////////////////////////////////////////////
	// setup markerRoots
	////////////////////////////////////////////////////////////

	// build markerControls
	markerRoot1 = new THREE.Group();
	scene.add(markerRoot1);
	let markerControls1 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot1, {
		type: 'pattern', patternUrl: "data/rabbit.patt",
	})
	markerRoot2 = new THREE.Group();
	scene.add(markerRoot2);
	let markerControls2 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot2, {
		type: 'pattern', patternUrl: "data/bird.patt",
	})
	markerRoot3 = new THREE.Group();
	scene.add(markerRoot3);
	let markerControls3 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot3, {
		type: 'pattern', patternUrl: "data/dragon.patt",
	})
	markerRoot4 = new THREE.Group();
	scene.add(markerRoot4);
	let markerControls4 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot4, {
		type: 'pattern', patternUrl: "data/fox.patt",
	})

	sceneGroup = new THREE.Group();

	let floorGeometry = new THREE.PlaneGeometry(20, 20);
	let floorMaterial = new THREE.ShadowMaterial();
	floorMaterial.opacity = 0.3;
	let floorMesh = new THREE.Mesh(floorGeometry, floorMaterial);
	floorMesh.rotation.x = -Math.PI / 2;
	floorMesh.receiveShadow = true;
	sceneGroup.add(floorMesh);

	mesh0 = doRabbitBall();
	mesh0.castShadow = true;
	mesh1 = doBirdBall();
	mesh1.castShadow = true;
	mesh2 = doFireBall();
	mesh2.castShadow = true;
	mesh3 = doFoxBall();
	mesh3.castShadow = true;

	let light = new THREE.PointLight(0xffffff, 1, 100);
	light.position.set(0, 4, 0); // default; light shining from top
	light.castShadow = true;
	sceneGroup.add(light);

	let lightSphere = new THREE.Mesh(
		new THREE.SphereGeometry(0.1),
		new THREE.MeshBasicMaterial({
			color: 0xffffff,
			transparent: true,
			opacity: 0.8
		})
	);
	lightSphere.position.copy(light.position);
	sceneGroup.add(lightSphere);
}

function doRabbitBall() {
	let geometry1 = new THREE.SphereGeometry(0.5, 32, 32);
	let material1 = new THREE.MeshPhongMaterial({
		map: THREE.ImageUtils.loadTexture( 'images/rabbitTexture.jpg')
	});

	let mesh = new THREE.Mesh(geometry1, material1);
	mesh.position.y = 0.5;
	return mesh;
}

function doBirdBall() {
	let geometry1 = new THREE.SphereGeometry(0.5, 32, 32);
	let material1 = new THREE.MeshPhongMaterial({
		map: THREE.ImageUtils.loadTexture( 'images/birdTexture.jpg')
	});

	let mesh = new THREE.Mesh(geometry1, material1);
	mesh.position.y = 0.5;
	return mesh;
}

function doFireBall() {
	let geometry1 = new THREE.SphereGeometry(0.5, 32, 32);
	let material1 = new THREE.MeshPhongMaterial({
		map: THREE.ImageUtils.loadTexture( 'images/fireTexture.jpg')
	});

	let mesh = new THREE.Mesh(geometry1, material1);
	mesh.position.y = 0.5;
	return mesh;
}

function doFoxBall() {
	let geometry1 = new THREE.SphereGeometry(0.5, 32, 32);
	let material1 = new THREE.MeshPhongMaterial({
		map: THREE.ImageUtils.loadTexture( 'images/foxTexture.jpg')
	});

	let mesh = new THREE.Mesh(geometry1, material1);
	mesh.position.y = 0.5;
	return mesh;
}

function update() {
	// update artoolkit on every frame
	if (arToolkitSource.ready !== false)
		arToolkitContext.update(arToolkitSource.domElement);
	if (markerRoot1.visible) {
		markerRoot1.add(mesh0);
		markerRoot1.add(sceneGroup);
		mesh0.position.y = 0.9 * (Math.abs(Math.sin(2.00 * totalTime + 0.10)) + 0.5);
	}
	if (markerRoot2.visible) {
		markerRoot2.add(mesh1);
		markerRoot2.add(sceneGroup);
		mesh1.position.y = 0.2 * (Math.abs(Math.sin(totalTime)) + 4);
		if (mesh1.position.x <= 6) {
			mesh1.position.x += 0.03
		}else {
			mesh1.position.x = mesh1.position.x - 6;
		}
		
	}
	if (markerRoot3.visible) {
		markerRoot3.add(mesh2);
		markerRoot3.add(sceneGroup);
		mesh2.position.y = (mesh2.position.y <= 10 ) ? mesh2.position.y += 0.1 : mesh2.position.y - 10;

	}
	if (markerRoot4.visible) {
		markerRoot4.add(mesh3);
		markerRoot4.add(sceneGroup);
		controlsOn = true;
	}else{
		controlsOn = false;
	}
}


function render() {
	renderer.render(scene, camera);
}


function animate() {
	requestAnimationFrame(animate);
	deltaTime = clock.getDelta();
	totalTime += deltaTime;
	update();
	render();
}
document.onkeydown = (e) => {
    switch (e.key) {
        case "w":
            if (controlsOn) { markerRoot4.children[0].position.z += 0.01; }
            break;
        case "s":
            if (controlsOn) { markerRoot4.children[0].position.z -= 0.01; }
            break;
        case "a":
            if (controlsOn) { markerRoot4.children[0].position.x += 0.01; }
            break;
        case "d":
            if (controlsOn) { markerRoot4.children[0].position.x -= 0.01; }
            break;
    }
}